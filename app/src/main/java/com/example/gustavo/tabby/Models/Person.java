package com.example.gustavo.tabby.Models;

/**
 * Created by gustavo on 16/03/18.
 */

public class Person {

    private String name;
    private Country country;

    public void Person(){

    }

    public Person(String name, Country country) {
        this.name = name;
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
