package com.example.gustavo.tabby.Fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.gustavo.tabby.Interfaces.onPersonCreated;
import com.example.gustavo.tabby.Models.Country;
import com.example.gustavo.tabby.Models.Person;
import com.example.gustavo.tabby.R;
import com.example.gustavo.tabby.Util.Util;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class FormFragment extends Fragment {

    public ArrayList<Country> countries;
    public EditText editTextPersonName;
    public Button btnPerson;
    public Spinner spinner;
    private onPersonCreated onPersonCreated;

    public FormFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_form, container, false);

        editTextPersonName = (EditText) view.findViewById(R.id.editTextPersonName);
        spinner = (Spinner) view.findViewById(R.id.countrySpinner);
        btnPerson = (Button) view.findViewById(R.id.buttonPerson);
        btnPerson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createNewPerson();
            }
        });

        countries =  Util.getCountries();

        ArrayAdapter<Country> adapter = new ArrayAdapter<Country>(getContext(), android.R.layout.simple_spinner_item,countries);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        return view;
    }

    public void createNewPerson() {
        if (!editTextPersonName.toString().isEmpty()) {
            Country country = (Country) spinner.getSelectedItem();
            Person person = new Person(editTextPersonName.toString(), country);
            onPersonCreated.createPerson(person);
        }
    }
    // Eventos para enlazar el listener
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof onPersonCreated) {
            onPersonCreated = (onPersonCreated) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnPersonCreated");
        }
    }

    // Eventos para desenlazar el listener
    @Override
    public void onDetach() {
        super.onDetach();
        onPersonCreated = null;
    }



}
