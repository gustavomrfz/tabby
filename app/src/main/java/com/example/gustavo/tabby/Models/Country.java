package com.example.gustavo.tabby.Models;

import java.text.MessageFormat;

/**
 * Created by gustavo on 16/03/18.
 */

public class Country {

    private String country;
    private String countryCode;


    public Country(String country, String countryCode) {
        this.country = country;
        this.countryCode = countryCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getFlagCountryURL(){
        return MessageFormat.format("http://www.geognos.com/api/en/countries/flag/{0}.png",
                this.getCountryCode());
    }

    @Override
    public String toString(){
        return country;
    }
}
