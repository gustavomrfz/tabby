package com.example.gustavo.tabby.Util;

import com.example.gustavo.tabby.Models.Country;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by gustavo on 16/03/18.
 */

public class Util {


    static ArrayList<Country> countries = new ArrayList<Country>();


    public static ArrayList<String> getCountryNames(){

        ArrayList<String> countryNames = new ArrayList<String>();

        List<Country> countries = createCountriesCodes();

        for (Country country: countries)
        {
            countryNames.add(country.getCountry());

        }

        return countryNames;

    }

    public static ArrayList<Country> getCountries(){

        ArrayList<Country> countryNames = new ArrayList<>();
        ArrayList<Country> countries = createCountriesCodes();

        for (Country country: countries)

                countryNames.add(country);
              {

        }

        return countryNames;

    }

    private static ArrayList<Country> createCountriesCodes () {

        ArrayList<Country> countries = new ArrayList<>();


        countries.add(new Country("España", "ES"));
        countries.add(new Country("México", "MX"));
        countries.add(new Country("Venezuela", "VE"));
        countries.add(new Country("Ecuador", "EC"));
        countries.add(new Country("Honduras", "HO"));
        countries.add(new Country("Perú", "PE"));
        countries.add(new Country("Uruguay", "UY"));
        countries.add(new Country("Chile", "CH"));

        return countries;
    }
}
