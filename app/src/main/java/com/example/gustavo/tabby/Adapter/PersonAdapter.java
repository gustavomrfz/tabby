package com.example.gustavo.tabby.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.gustavo.tabby.Models.Person;
import com.example.gustavo.tabby.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by gustavo on 16/03/18.
 */

public class PersonAdapter extends BaseAdapter {

    private Context context;
    private int layout;
    private List<Person> persons;

    public PersonAdapter() {

    }

    public PersonAdapter(Context context, int layout, List<Person> persons) {
        this.context = context;
        this.layout = layout;
        this.persons = persons;
    }

    @Override
    public int getCount() {
        return persons.size();
    }

    @Override
    public Person getItem(int position) {
        return this.persons.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(this.context).inflate(this.layout,null);
            holder = new ViewHolder();
            holder.name= (TextView) convertView.findViewById(R.id.textViewPersonName);
            holder.flag = (ImageView) convertView.findViewById(R.id.imageViewCountryFlag);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Person currentPerson = getItem(position);

        holder.name.setText(currentPerson.getName());
        String url = currentPerson.getCountry().getFlagCountryURL();
        Picasso.get().load(url).into(holder.flag);

        return convertView;
    }

    static class ViewHolder{
        private TextView name;
        private ImageView flag;
    }
}
