package com.example.gustavo.tabby.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ListView;

import com.example.gustavo.tabby.Adapter.PersonAdapter;
import com.example.gustavo.tabby.Models.Person;
import com.example.gustavo.tabby.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListFragment extends Fragment {

    private ArrayList<Person> persons;
    private ListView listViewPersons;
    private PersonAdapter adapter;

    public ListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_list, container, false);
        persons = new ArrayList<Person>();
        listViewPersons = (ListView) view.findViewById(R.id.listViewPersons);
        adapter = new PersonAdapter(getContext(), R.layout.person_item, persons);
        listViewPersons.setAdapter(adapter);

        return view;
    }

    public void addPerson(Person person) {
        this.persons.add(person);
        adapter.notifyDataSetChanged();
    }
}
